package com.techuniversity.team5project.model;

import com.techuniversity.team5project.domain.entities.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientRequest {
    private String dni;
    private String name;
    private String surname;
    private String address;
    private int phoneNumber;

}
