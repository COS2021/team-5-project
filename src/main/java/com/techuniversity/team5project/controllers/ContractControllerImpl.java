package com.techuniversity.team5project.controllers;

import com.techuniversity.team5project.controllers.interfaces.ContractController;
import com.techuniversity.team5project.domain.entities.Client;
import com.techuniversity.team5project.model.ClientRequest;
import com.techuniversity.team5project.model.ClientResponse;
import com.techuniversity.team5project.model.ContractResponse;
import com.techuniversity.team5project.services.ContractService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api
@RestController
@RequestMapping("/contract")
public class ContractControllerImpl implements ContractController {

    private final ContractService contractService;

    @Autowired
    public ContractControllerImpl(ContractService ContractService) {
        this.contractService = ContractService;
    }


    @GetMapping(value = "/dni")
    public ContractResponse getContract(String dni) {
        return contractService.getContractResponse(dni);
    }

    @PostMapping(value = "/")
    public ContractResponse createContract(ClientRequest clientRequest) {
        return contractService.createContract(clientRequest);
    }

    @DeleteMapping(value = "/dni/delete")
    public void deleteClient(String dni) {
        contractService.deleteClient(dni);
    }

    @PutMapping("/put")
    public ClientResponse updateClient(ClientRequest clientRequest) {
        return contractService.updateClientResponse(clientRequest);
    }
    /*@GetMapping(value = "/getAll")
    public List<ContractResponse> getAll(){
        return contractService.getContractResponses();
    }*/


/*
    @PutMapping("/put")
    public ContractResponse updateEntity(ContractRequest contractRequest) {
        return contractService.updateContractResponse(contractRequest);
    }

*/

}
