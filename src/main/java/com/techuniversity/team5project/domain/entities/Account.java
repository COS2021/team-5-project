package com.techuniversity.team5project.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "Account")
public class Account {
    @JsonIgnore
    private ObjectId id;
    private String cclient;
    private String bank;
    private String branch;
    private String counterpart;
    private int sheet;

}
