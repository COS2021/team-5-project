package com.techuniversity.team5project.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import nonapi.io.github.classgraph.json.Id;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Document(collection = "Client")
public class Client {
    @JsonIgnore
    private ObjectId id;
    private String dni;
    private String name;
    private String surname;
    private String address;
    private int phoneNumber;
    private String cclient;
}
