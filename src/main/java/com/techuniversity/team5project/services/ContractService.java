package com.techuniversity.team5project.services;

import com.techuniversity.team5project.domain.entities.Account;
import com.techuniversity.team5project.domain.entities.Client;
import com.techuniversity.team5project.model.ClientRequest;
import com.techuniversity.team5project.model.ClientResponse;
import com.techuniversity.team5project.model.ContractResponse;
import com.techuniversity.team5project.repositories.ClientRepository;
import com.techuniversity.team5project.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class ContractService {
    private final ClientRepository clientRepository;
    private final AccountRepository accountRepository;

    @Autowired
    public ContractService(ClientRepository clientRepository, AccountRepository accountRepository) {
        this.clientRepository = clientRepository;
        this.accountRepository = accountRepository;
    }

    public ContractResponse getContractResponse(String dni) {
        Client client = clientRepository.findByDni(dni);
        Account account = accountRepository.findByCclient(client.getCclient());
        return ContractResponse.builder()
                .name(client.getName())
                .surname(client.getSurname())
                .address(client.getAddress())
                .phoneNumber(client.getPhoneNumber())
                .bank(account.getBank())
                .counterpart(account.getCounterpart())
                .branch(account.getBranch())
                .sheet(account.getSheet())
                .build();
    }


    public ContractResponse createContract(ClientRequest clientRequest) {
        Client clientNew = clientRepository.save(createNewClient(clientRequest));
        Account accountNew = accountRepository.save(createNewAccount(clientNew));
        return ContractResponse.builder()
                .name(clientNew.getName())
                .surname(clientNew.getSurname())
                .address(clientNew.getAddress())
                .phoneNumber(clientNew.getPhoneNumber())
                .bank(accountNew.getBank())
                .branch(accountNew.getBranch())
                .counterpart(accountNew.getCounterpart())
                .sheet(accountNew.getSheet())
                .build();

    }

    public Client createNewClient(ClientRequest clientRequest) {
        Random random = new Random();

        int randomCclient = random.nextInt(999999999);
        String cclient = String.valueOf(randomCclient);

        return Client.builder().dni(clientRequest.getDni())
                .name(clientRequest.getName())
                .surname(clientRequest.getSurname())
                .address(clientRequest.getAddress())
                .phoneNumber(clientRequest.getPhoneNumber())
                .cclient(cclient)
                .build();
    }


    public Account createNewAccount(Client client) {
        Random random = new Random();

        int randomSheet = random.nextInt(9999999);

        return Account.builder()
                .cclient(client.getCclient())
                .bank("0182")
                .branch("0001")
                .counterpart("0020")
                .sheet(randomSheet)
                .build();
    }

    public void deleteClient(String dni) {

        Client deleteClient = clientRepository.findByDni(dni);
        clientRepository.deleteByDni(dni);
        accountRepository.deleteByCclient(deleteClient.getCclient());
    }


    public ClientResponse updateClientResponse(ClientRequest clientRequest) {
        Client client = clientRepository.findByDni(clientRequest.getDni());
        client.setDni(clientRequest.getDni());
        client.setName(clientRequest.getName());
        client.setSurname(clientRequest.getSurname());
        client.setAddress(clientRequest.getAddress());
        client.setPhoneNumber(clientRequest.getPhoneNumber());
        return mapClientToResponse(clientRepository.save(client));
    }

    private ClientResponse mapClientToResponse(Client client) {
        return ClientResponse.builder().name(client.getDni()).build();

    }

    /*public ClientResponse getClientResponse(ClientRequest clientRequest, AccountRequest accountRequest) {
        Client client = clientRepository.findByDniAndBank(clientRequest.getDni(), clientRequest.getBank());
        Account account = accountRepository.findByCclientAndBank(accountRequest.)
        return mapClientToResponse(client);
    }

    private ContractResponse mapToResponse(Client client, Account account) {


    }*/
    /*private ContractResponse mapToResponse(ClientRequest clientRequest, AccountRequest accountRequest) {
        return ClientResponse
                .builder()
                .name(client.getName())
                .surname(client.getSurname())
                .address(client.getAddress())
                .phoneNumber(client.getPhoneNumber())
                .cclient(client.getCclient()).build();

    }*/
}
