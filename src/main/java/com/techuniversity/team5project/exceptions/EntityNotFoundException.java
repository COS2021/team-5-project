package com.techuniversity.team5project.exceptions;

import com.techuniversity.team5project.Utils.ErrorMessagesEnum;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class EntityNotFoundException extends BaseException {

    public EntityNotFoundException(Class clazz, String... searchParamsMap) {
        super(
                EntityNotFoundException.generateMessage(clazz.getSimpleName(),
                        toMap((Object) searchParamsMap))
        );
    }

    private static String generateMessage(String entity, Map<String, String> searchParams) {
        return StringUtils.capitalize(entity) +
                ErrorMessagesEnum.NOT_FOUND_PARAMEATERS.getMessage() +
                searchParams;
    }

    private static <K, V> Map<K, V> toMap(Object... entries) {
        if (entries.length % 2 == 1) {
            throw new IllegalArgumentException("Invalid entries");
        }
        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(
                        HashMap::new,
                        (m, i) -> m.put(((Class<K>) String.class).cast(entries[i]), ((Class<V>) String.class).cast(entries[i + 1])),
                        Map::putAll);
    }
}